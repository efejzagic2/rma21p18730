package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class GrupaListViewModel : ViewModel() {
//

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getAllDB(context: Context, onSuccess: (movies: List<Grupa>) -> Unit,
                 onError: () -> Unit){

        scope.launch{
            val result = GrupaRepository.getAllDB(context)
            when (result) {
                is List<Grupa> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }



//    fun writeDB(context: Context, grupa:Grupa, onSuccess: (movies: String) -> Unit,
//                onError: () -> Unit){
//        scope.launch{
//            val result = GrupaRepository.writeAll(context,grupa)
//            when (result) {
//                is String -> onSuccess?.invoke(result)
//                else-> onError?.invoke()
//            }
//        }
//    }


    suspend fun getAll() :List<Grupa> ?{
        return GrupaRepository.getAll()
    }

    suspend fun getGrupeByKviz(idKviza: Int) : List<Grupa> {
        return GrupaRepository.getGrupeByKviz(idKviza)
    }

    suspend fun getGrupeForStudent(acHash: String) : List<Grupa> {
        return GrupaRepository.getGrupeForStudent(acHash)
    }

    suspend fun getGrupaById(idGrupe: Int) : Grupa {
        return GrupaRepository.getGrupaById(idGrupe)
    }

    suspend fun getGrupeByPredmet(idPredmeta: Int) : List<Grupa> {
        return GrupaRepository.getGrupeByPredmet(idPredmeta)
    }

    suspend fun addGrupa(gid: Int) {
        GrupaRepository.addGrupa(gid)
    }

    suspend fun getGrupaByName(ime: String): Grupa? {
        return GrupaRepository.getGrupaByName(ime)
    }


//    fun getUpisaneGrupe() : List<Grupa> {
//        return GrupaRepository.upisaneGrupe
//    }
//
//    fun getUpisanuGrupuZaPredmet(predmet: String) : Grupa? {
//        return GrupaRepository.getUpisanuGrupuByPredmet(predmet)
//    }

}