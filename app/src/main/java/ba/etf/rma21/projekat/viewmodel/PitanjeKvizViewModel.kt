package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository

class PitanjeKvizViewModel : ViewModel(){

//    fun getPitanjaKviz() : List<PitanjeKviz> {
//        return PitanjeKviz.pitanjeKvizLista
//    }
//
//    fun getPitanjeKvizByKviz(kviz: String?) : List<PitanjeKviz> {
//        return PitanjeKviz.getPitanjeKvizByKviz(kviz)
//    }
//

    suspend fun getPitanja(idKviza: Int): List<Pitanje> ?{
        return PitanjeKvizRepository.getPitanja(idKviza)
    }

}