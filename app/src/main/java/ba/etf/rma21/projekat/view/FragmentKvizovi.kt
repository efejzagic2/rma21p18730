package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.viewmodel.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FragmentKvizovi :Fragment() {

    private lateinit var kvizovi: RecyclerView

    private  var  kvizListViewModel: KvizListViewModel = KvizListViewModel()
    private lateinit var filterKvizova: Spinner
    private var kvizoviArrayList: List<Kviz> = listOf()
    private var pitanjeListViewModel = PitanjeListViewModel()
    private var predmetListViewModel = PredmetiListViewModel()
    private var listaFragmenata: MutableList<FragmentPokusaj> = mutableListOf()
    private lateinit var fragmentPokusaj: FragmentPokusaj
    private var listaKvizova: MutableList<Kviz> = mutableListOf()
    private var grupaListViewModel = GrupaListViewModel()
    private var predmetIGrupaListViewModel = PredmetIGrupaListViewModel()
    private var pitanjeKvizViewModel = PitanjeKvizViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.Main)
    var fragmentsLoaded: Boolean =false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.kvizovi_fragment, container, false)

        (activity as MainActivity?)!!.navBarKvizoviIPredmeti()

//
//        context?.let {
//            kvizListViewModel.getAllKvizDB(
//                it,onSuccess = ::onSuccess,
//                onError = ::onError)
//        }



        kvizovi = view.findViewById(R.id.listaKvizova)
        filterKvizova = view.findViewById(R.id.filterKvizova)
        kvizovi.layoutManager = GridLayoutManager(context, 2)


        kvizovi.adapter=KvizListAdapter(arrayListOf()) {kviz -> showKvizDetails(kviz)}

        filterKvizova.adapter = ArrayAdapter(view.context, R.layout.support_simple_spinner_dropdown_item, listOf(
            "Svi moji kvizovi",
            "Svi kvizovi",
            "Urađeni kvizovi",
            "Budući kvizovi",
            "Prošli kvizovi"
        ))

        filterKvizova.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {

                scope.launch {
                    kvizoviArrayList = kvizListViewModel.odaberiKviz(pos)!!

                    kvizovi.adapter = KvizListAdapter(kvizoviArrayList) { kviz ->
                        scope.launch {
                            for (l in kvizListViewModel.getUpisani()!!) {


                                if(!provjera(FragmentPokusaj(pitanjeKvizViewModel.getPitanja(kviz.id)!!)))
                                    listaFragmenata.add(FragmentPokusaj(pitanjeKvizViewModel.getPitanja(kviz.id)!!))


                                listaKvizova.add(kvizListViewModel.getKvizById(kviz.id)!!)

                                //  listaKvizova = kvizListViewModel.getAll() as MutableList<Kviz>
                               // if (kviz.id == l.id)
                                    showKvizDetails(kviz)
                            }
                        }
                    }
                }
            }
        }
        return view;
    }


//    fun onSuccess(kviz:List<Kviz>){
//       // kviz.updateMovies(movies)
//        val toast = Toast.makeText(context, "Success", Toast.LENGTH_SHORT)
//        toast.show()
//    }
//    fun onError() {
//        val toast = Toast.makeText(context, "Error", Toast.LENGTH_SHORT)
//        toast.show()
//    }

    fun provjera(fragmentPokusaj: FragmentPokusaj) : Boolean {

        for(i in listaFragmenata) {
            if(i == fragmentPokusaj)
                return true
        }
        return false
    }



    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    override fun onResume() {
        super.onResume()
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        scope.launch {
            kvizovi.adapter = KvizListAdapter(kvizListViewModel.odaberiKviz(0)!!) {kviz -> showKvizDetails(kviz)}
        }
        (activity as MainActivity).navigationView.setOnNavigationItemSelectedListener((activity as MainActivity).mOnNavigationItemSelectedListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
            }
        })
    }

    private fun showKvizDetails(kviz: Kviz) {
        val bundle = Bundle()
        bundle.putString("naziv", kviz.naziv) //bio prije nazivPredmeta
        var position=0

        for (i in listaKvizova) {
            if (kviz.id == i.id) {
                break
            }
            position++
        }
        listaFragmenata[position].setArguments(bundle);
        openFragment(listaFragmenata[position])
    }


    private fun openFragment(fragment: Fragment) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}