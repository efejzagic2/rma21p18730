package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetRepository

class PredmetiListViewModel : ViewModel() {

    suspend fun getPredmete() : List<Predmet> {
        return PredmetRepository.getAll()
    }

    suspend fun getPredmetId(idPredmeta: Int): Predmet {
        return PredmetRepository.getPredmetId(idPredmeta)
    }
//    fun getPredmete(god: Int): List<String> {
//        return PredmetRepository.getPredmetsByGodina(god).map {
//            it.naziv
//        }.toList()
//    }
//
//    fun getUpisanePredmete() : List<Predmet> {
//        return PredmetRepository.getUpisani()
//    }
//
//    fun getGrupe(predmet: String): List<String> {
//        return GrupaRepository.getGroupsByPredmet(predmet).map {
//            it.naziv
//        }.toList()
//    }

}