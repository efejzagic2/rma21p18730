//package ba.etf.rma21.projekat.data.models
//
//import com.google.gson.Gson
//import com.google.gson.reflect.TypeToken
//import java.lang.reflect.Type
//
//annotation class TypeConverter {
//
//    @TypeConverter
//    fun fromTransactionList(transaction: List<String?>?): String? {
//        if (transaction == null) {
//            return null
//        }
//        val gson = Gson()
//        val type: String = object : TypeToken<List<String?>?>() {}.type
//        return gson.toJson(transaction, type)
//    }
//
//    @TypeConverter
//    fun toTransactionList(transactionString: String?): List<String>? {
//        if (transactionString == null) {
//            return null
//        }
//        val gson = Gson()
//        val type =
//            object : TypeToken<List<String?>?>() {}.type
//        return gson.fromJson<List<String>>(transactionString, type)
//    }
//
//}