//package ba.etf.rma21.projekat
//
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.UtilTestClass.Companion.hasItemCount
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import org.hamcrest.CoreMatchers
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//
//@RunWith(AndroidJUnit4::class)
//class KvizRepoTest {
//
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun getAllKvizes() {
//
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Svi kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        var brojKvizova = KvizRepository.getAll()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//    }
//
//    @Test
//    fun getMyKvizes() {
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Svi moji kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        var brojKvizova = KvizRepository.getMyKvizes()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//    }
//
//    @Test
//    fun getDone() {
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Urađeni kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        var brojKvizova = KvizRepository.getDone()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//    }
//
//    @Test
//    fun getFuture() {
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Budući kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        var brojKvizova = KvizRepository.getFuture()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//    }
//
//    @Test
//    fun getNotTaken() {
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Prošli kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        var brojKvizova = KvizRepository.getNotTaken()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//    }
//
//}