package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GrupaRepository {


   companion object {
//
//        var upisaneGrupe : MutableList<Grupa> = ArrayList()
//        var neupisaneGrupe : MutableList<Grupa> = ArrayList()
//
//        init {
//
//            //podaci
//            upisaneGrupe.add(Grupa("Grupa 1", "RMA"))
//            upisaneGrupe.add(Grupa("Grupa 1", "TP"))
//
//            neupisaneGrupe.add(Grupa("Grupa 2", "TP"))
//            neupisaneGrupe.add(Grupa("Grupa 3", "TP"))
//
//            neupisaneGrupe.add(Grupa("Grupa 2", "RMA"))
//            neupisaneGrupe.add(Grupa("Grupa 3", "RMA"))
//
//            neupisaneGrupe.add(Grupa("Grupa 1" , "IM2"))
//            neupisaneGrupe.add(Grupa("Grupa 2" , "IM2"))
//
//            neupisaneGrupe.add(Grupa("Grupa 1" , "WT"))
//            neupisaneGrupe.add(Grupa("Grupa 2" , "WT"))
//            neupisaneGrupe.add(Grupa("Grupa 3" , "WT"))
//
//            neupisaneGrupe.add(Grupa("Grupa 1" , "OOAD"))
//            neupisaneGrupe.add(Grupa("Grupa 2" , "OOAD"))
//            neupisaneGrupe.add(Grupa("Grupa 3" , "OOAD"))
//
//            neupisaneGrupe.add(Grupa("Grupa 1" , "SI"))
//            neupisaneGrupe.add(Grupa("Grupa 2" , "SI"))
//            neupisaneGrupe.add(Grupa("Grupa 3" , "SI"))
//
//        }
//

       private lateinit var context:Context
       fun setContext(_context: Context){
           context=_context
       }

       suspend fun getAllDB(context: Context) : List<Grupa> {
           return withContext(Dispatchers.IO) {
               var db = AppDatabase.getInstance(context)
               var grupe = db!!.grupaDao().getAll()
               return@withContext grupe
           }
       }
        suspend fun getAll(): List<Grupa> ?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllGrupa()
                return@withContext response
            }
        }


//       suspend fun writeAll(context: Context,grupa: Grupa) : String?{
//           return withContext(Dispatchers.IO) {
//               try{
//                   var db = AppDatabase.getInstance(context)
//                   db!!.grupaDao().insertAll(grupa)
//                   return@withContext "success"
//               }
//               catch(error:Exception){
//                   return@withContext null
//               }
//           }
//       }
//


//        fun getGroupsByPredmet(nazivPredmeta: String): MutableList<Grupa> {
//            var moguceGrupe : MutableList<Grupa> = ArrayList()
//
//            for(grupa in neupisaneGrupe) {
//                if (nazivPredmeta == grupa.nazivPredmeta)
//                    moguceGrupe.add(grupa)
//            }
//            return moguceGrupe
//        }
//
//        fun getUpisanuGrupuByPredmet(predmet: String) : Grupa? {
//            return upisaneGrupe.find {
//                it.nazivPredmeta==predmet
//            }
//        }
//
       suspend fun getGrupeByKviz(idKviza: Int): List<Grupa> {
           return withContext(Dispatchers.IO) {
               var response = ApiAdapter.retrofit.getGrupeByKviz(idKviza)
               return@withContext response
           }
       }

       suspend fun getGrupeForStudent(acHash: String): List<Grupa> {
           return withContext(Dispatchers.IO) {
               var response = ApiAdapter.retrofit.getGrupeForStudent(acHash)
               return@withContext response
           }
       }

       suspend fun getGrupaById(idGrupe: Int): Grupa {
           return withContext(Dispatchers.IO) {
               var response = ApiAdapter.retrofit.getGrupaById(idGrupe)
               return@withContext response
           }
       }

       suspend fun getGrupeByPredmet(idPredmeta: Int): List<Grupa> {
           return withContext(Dispatchers.IO) {
               var response = ApiAdapter.retrofit.getGrupeByPredmet(idPredmeta)
               return@withContext response
           }
       }

       suspend fun addGrupa(gid: Int): Boolean? {
           return withContext(Dispatchers.IO) {
               ApiAdapter.retrofit.addGrupa(gid, AccountRepository.getHash())
               return@withContext true
           }
       }

        suspend fun getGrupaByName(ime: String): Grupa? {
            var grupe = getAll() //ispraviti
            return grupe?.firstOrNull {
                it.naziv==ime
            }
        }



//        //dodavanje grupa u listu Upisanih
//        fun addUpisani(grupa: String, predmet: String) {
//            upisaneGrupe.add(Grupa(grupa, predmet))
//        }
    }
}