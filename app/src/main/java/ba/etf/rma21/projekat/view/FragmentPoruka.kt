package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R

class FragmentPoruka : Fragment() {

    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.poruka_fragment, container, false)

        var tvPoruka: TextView = view.findViewById(R.id.tvPoruka)

        val bundle: Bundle  = requireArguments()
        if(bundle.getString("fragment").toString().equals("pokusaj")) {
            tvPoruka.setText("Završili ste kviz " + bundle.getString("kviz") + " sa tačnosti " + bundle.getDouble("procenat"))
        }
        else {
            tvPoruka.setText("Uspješno ste upisani u grupu " + bundle.getString("grupa") + " predmeta " + bundle.getString("predmet") + "!")
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                openFragment(FragmentKvizovi())
            }
        })
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}