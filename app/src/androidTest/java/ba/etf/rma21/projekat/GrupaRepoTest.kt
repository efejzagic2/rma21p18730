//package ba.etf.rma21.projekat
//
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.UtilTestClass.Companion.hasItemCount
//import ba.etf.rma21.projekat.data.repositories.GrupaRepository
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import kotlinx.coroutines.runBlocking
//import org.hamcrest.CoreMatchers
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//
//@RunWith(AndroidJUnit4::class)
//class GrupaRepoTest {
//
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//
//    @Test
//    fun getGroupsByPredmet() = runBlocking {
//
//        var brojKvizova = KvizRepository.getMyKvizes()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("2")
//                )
//        ).perform(ViewActions.click())
//
//
//        Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).perform(ViewActions.click())
//
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("OOAD")
//                )
//        ).perform(ViewActions.click())
//
//
//        for(a in GrupaRepository.getGrupeByPredmet(1)) {
//            Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).perform(ViewActions.click())
//
//            Espresso.onData(
//                    CoreMatchers.allOf(
//                            CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                            CoreMatchers.`is`(a.naziv)
//                    )
//            ).perform(ViewActions.click())
//        }
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.kvizovi)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Svi moji kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size + 1))
//
//    }
//
//
//    @Test
//    fun getPredmetiForGodine() = runBlocking  {
//
//
//        var brojKvizova = KvizRepository.getMyKvizes()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size))
//
//
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//
//
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("1")
//                )
//        ).perform(ViewActions.click())
//
//
//        for(a in PredmetIGrupaRepository.get(1)) {
//            Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).perform(ViewActions.click())
//
//            Espresso.onData(
//                    CoreMatchers.allOf(
//                            CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                            CoreMatchers.`is`(a.naziv)
//                    )
//            ).perform(ViewActions.click())
//        }
//
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.kvizovi)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(
//                CoreMatchers.allOf(
//                        CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`("Svi moji kvizovi")
//                )
//        ).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).check(hasItemCount(brojKvizova.size + 1))
//
//    }
//}