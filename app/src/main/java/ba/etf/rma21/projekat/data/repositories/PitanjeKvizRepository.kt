package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PitanjeKvizRepository {

    companion object {
//
//
//        private var pitanjeKvizViewModel = PitanjeKvizViewModel()
//        private var pitanja : MutableList<Pitanje> = ArrayList()
//        private var pitanjaKviz = pitanjeKvizViewModel.getPitanjaKviz()
//
//        init {
//            pitanja = giveItToMe() as MutableList<Pitanje>
//        }
//
//         fun giveItToMe(): List<Pitanje> {
//            return listOf(
//                    Pitanje("Pitanje 1", "Pitanje broj 1", listOf(
//                            "Tacan odgovor",
//                            "RMA TP",
//                            "Netacan odgovor",
//                            "Netacan odgovor"
//                    ), 1),
//
//                    Pitanje("Pitanje 2", "Pitanje broj 2", listOf(
//                            "Netacan odgovor",
//                            "Tacan odgovor",
//                            "RMA TP",
//                            "Netacan odgovor"
//                    ), 2),
//                    Pitanje("Pitanje 3", "Pitanje broj 3", listOf(
//                            "Netacan odgovor",
//                            "Netacan odgovor",
//                            "Tacan odgovor",
//                            "RMA TP"
//                    ), 3),
//                    Pitanje("Pitanje 4", "Pitanje broj 4", listOf(
//                            "Netacan odgovor",
//                            "RMA",
//                            "Netacan odgovor",
//                            "Tacan odgovor"
//                    ), 4),
//
//                    Pitanje("Pitanje 5", "Pitanje broj 5", listOf(
//                            "Netacan odgovor",
//                            "Netacan odgovor",
//                            "Samo TP",
//                            "Tacan odgovor"
//                    ), 4)
//            )
//        }
//
//        fun getPitanja(naziv: String, nazivPredmeta: String): List<Pitanje> {
//            return pitanja.filter {
//                pitanjaKviz.firstOrNull {it2: PitanjeKviz ->
//                    it2.kviz == naziv && it2.kviz==nazivPredmeta && it2.naziv == it.naziv
//
//                } !=null
//            }.toList()
//        }
//
//        fun getPitanjaSort(): List<Pitanje> {
//            return pitanja
//        }
//
//        fun getPitanjeByNaziv(naziv: String): Pitanje? {
//            return pitanja.find {
//                it.naziv == naziv
//            }
//        }


        //spirala 4

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }


//        //spirala 3 funkcija
        suspend fun getPitanja(idKviza:Int):List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                return@withContext ApiAdapter.retrofit.getPitanja(idKviza)
            }
        }
    }
}