package ba.etf.rma21.projekat

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    lateinit var navigationView: BottomNavigationView

    private var kvizoviFragment: Fragment = FragmentKvizovi.newInstance()
    private var predmetiFragment: Fragment = FragmentPredmeti.newInstance()



    val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                openFragment(kvizoviFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                openFragment(predmetiFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun getNavBar() : BottomNavigationView {
        return navigationView
    }

    fun navBarPokusaj() {
        navigationView.menu.setGroupVisible(R.id.prvi, false)
        navigationView.menu.setGroupVisible(R.id.drugi, true)
    }

    fun navBarKvizoviIPredmeti() {
        navigationView.menu.setGroupVisible(R.id.prvi, true)
        navigationView.menu.setGroupVisible(R.id.drugi, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)


        AccountRepository.setContext(this)

        val scope = CoroutineScope(Job() + Dispatchers.Main)

        val uri = intent
        if (uri != null) {
            val hash = uri.getStringExtra("payload")
            if (hash != null) {
                AccountViewModel.postaviHash(hash)
                val toast = Toast.makeText(this, "Trenutni hash korisnika: " + hash, Toast.LENGTH_SHORT)
                toast.show()
            }

        }
        else {
            scope.launch {
                 AccountRepository.postaviHash(AccountRepository.acHash)
            }

            val toast = Toast.makeText(this, "Trenutni hash korisnika: " + AccountRepository.acHash, Toast.LENGTH_SHORT)
            toast.show()
        }

        navigationView = findViewById(R.id.bottomNav)
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigationView.selectedItemId = R.id.kvizovi

    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}



//stari kod

    /*private lateinit var listaKvizova: RecyclerView
    private lateinit var  listaKvizovaAdapter: KvizListAdapter
    private lateinit var upisDugme: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listaKvizova = findViewById(R.id.listaKvizova)
        listaKvizova.layoutManager = GridLayoutManager(this, 2)

        listaKvizovaAdapter = KvizListAdapter(KvizRepository.getAll())
        listaKvizova.adapter = listaKvizovaAdapter

        //spinner
        val filterKvizova: Spinner = findViewById(R.id.filterKvizova)
        ArrayAdapter.createFromResource(this, R.array.filter_kvizova, android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            filterKvizova.adapter = adapter
        }


        filterKvizova.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                sortiraj(pos)
            }

            override fun onNothingSelected(parent: AdapterView<out Adapter>?) {
            }
        }

        //new action button
        upisDugme = findViewById(R.id.upisDugme)
        upisDugme.setOnClickListener {
            val intent = Intent(this@MainActivity, UpisPredmet::class.java)
            startActivity(intent)
        }

    }

    private fun sortiraj(pos: Int) {
        if(pos==0) {
            listaKvizovaAdapter = KvizListAdapter(KvizRepository.getMyKvizes())
            listaKvizova.adapter = listaKvizovaAdapter
            listaKvizovaAdapter.notifyDataSetChanged()
        }
        else if(pos==1) {
            listaKvizovaAdapter = KvizListAdapter(KvizRepository.getAll())
            listaKvizova.adapter = listaKvizovaAdapter
            listaKvizovaAdapter.notifyDataSetChanged()
        }
        else if(pos==2) {
            listaKvizovaAdapter = KvizListAdapter(KvizRepository.getDone())
            listaKvizova.adapter = listaKvizovaAdapter
            listaKvizovaAdapter.notifyDataSetChanged()
        }
        else if(pos==3) {
            listaKvizovaAdapter = KvizListAdapter(KvizRepository.getFuture())
            listaKvizova.adapter = listaKvizovaAdapter
            listaKvizovaAdapter.notifyDataSetChanged()
        }
        else if(pos==4) {
            listaKvizovaAdapter = KvizListAdapter(KvizRepository.getNotTaken())
            listaKvizova.adapter = listaKvizovaAdapter
            listaKvizovaAdapter.notifyDataSetChanged()
        }
    }
}

     */
