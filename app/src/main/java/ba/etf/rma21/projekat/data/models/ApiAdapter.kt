package ba.etf.rma21.projekat.data.models

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiAdapter {
    val retrofit : Api = Retrofit.Builder()
        .baseUrl("https://rma21-etf.herokuapp.com")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(Api::class.java)
}


//package ba.etf.rma21.projekat.data.models
//
//import ba.etf.rma21.projekat.data.repositories.ApiConfig
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import java.util.concurrent.TimeUnit
//
//object ApiAdapter {
//    private val interceptor = run {
//        val httpLoggingInterceptor = HttpLoggingInterceptor()
//        httpLoggingInterceptor.apply {
//            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
//        }
//    }
//
//
//    private val okHttpClient = OkHttpClient.Builder()
//        .addInterceptor(interceptor) // same for .addInterceptor(...)
//        .connectTimeout(30, TimeUnit.SECONDS) //Backend is really slow
//        .writeTimeout(30, TimeUnit.SECONDS)
//        .readTimeout(30, TimeUnit.SECONDS)
//        .build()
//
//    val retrofit : Api = Retrofit.Builder()
//        .baseUrl(ApiConfig.baseURL)
//        .addConverterFactory(GsonConverterFactory.create())
//        .client(okHttpClient)
//        .build()
//        .create(Api::class.java)
//}