package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository

class PredmetIGrupaListViewModel {

    suspend fun getPredmete() : List<Predmet> ?{
        return PredmetIGrupaRepository.getPredmeti()
    }

    suspend fun getGrupe() : List<Grupa> ?{
        return PredmetIGrupaRepository.getGrupe()
    }


}