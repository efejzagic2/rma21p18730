package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account



@Dao
interface AccountDao {

    @Query("SELECT * from Account")
    suspend fun getAccount(): Account

    @Query("DELETE from Account")
    suspend fun izbrisiSve()

    @Insert
    suspend fun insertAccount(account: Account)
}