package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DBRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context) {
            context = _context
        }

//
//        suspend fun updateNow(): Boolean {
////            return withContext(Dispatchers.IO) {
////                var db = AppDatabase.getInstance(context)
////                val trenutniAccount = db!!.accountDao.getAccount()
////                val promjenjena = getPromjene(AccountRepository.getHash(), trenutniAccount.lastUpdate)
////
////                if(promjenjena.changed) {
////
////                }
////
////                return@withContext promjenjena.changed
////            }
//            //}
//            return false
//        }


        suspend fun updateNow () : Boolean {
        return withContext(Dispatchers.IO) {
            val trenutniAccount = AccountRepository.getAccountByHash(AccountRepository.getHash())

            var promjenjena = getPromjene(AccountRepository.getHash(), trenutniAccount.lastUpdate)
            if(promjenjena) {
                izbrisiSveIzBaze()
                upisiSveUBazu()

                return@withContext true
            }
            return@withContext false
            }
        }



        suspend fun getPromjene(acHash:String, lastUpdate: String): Boolean {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.isChanged(acHash,lastUpdate)
                return@withContext response
            }
        }


        suspend fun izbrisiSveIzBaze() {
            var db = AppDatabase.getInstance(context)
            //db!!.accountDao().izbrisiSve()
            db!!.grupaDao().izbrisiSve()
            db!!.kvizDao().izbrisiSve()
            db!!.pitanjeDao().izbrisiSve()
            db!!.predmetDao().izbrisiSve()
            db!!.odgovorDao().izbrisiSve()
        }

        suspend fun upisiSveUBazu() {
            var db = AppDatabase.getInstance(context)
            var sviKvizovi = ApiAdapter.retrofit.getAllKviz()

            for(i in sviKvizovi) {
                db!!.kvizDao().insertKviz(i)
            }

            var sveGrupe = ApiAdapter.retrofit.getAllGrupa()

            for (i in sveGrupe) {
                db!!.grupaDao().insertGrupa(i)
            }


            var sviPredmeti = ApiAdapter.retrofit.getAllPredmet()

            for(i in sviPredmeti) {
                db!!.predmetDao().insertPredmet(i)
            }

            for (i in sviKvizovi) {
                var svaPitanja = ApiAdapter.retrofit.getPitanja(i.id)
                for(j in svaPitanja) {
                    db!!.pitanjeDao().insertPitanje(j)
                }
            }

            for (i in sviKvizovi) {
                var sviOdgovori = ApiAdapter.retrofit.getOdgovoriForStudent(AccountRepository.getHash(),i.id)
                for(j in sviOdgovori) {
                    db!!.odgovorDao().insertOdgovor(j)
                }
            }


        }


    }
}