package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository

class PitanjeListViewModel: ViewModel() {

    private var pitanjeKvizViewModel = PitanjeKvizViewModel()

//    fun getPitanjaSort(naziv: String, nazivPredmeta: String) : List<Pitanje> {
//        return PitanjeKvizRepository.getPitanja(naziv, nazivPredmeta)
//    }
//
//    fun getPitanja() : List<Pitanje> {
//        return PitanjeKvizRepository.getPitanjaSort()
//    }
//
//    fun getPitanjaByNaziv(naziv: String) : Pitanje? {
//        return PitanjeKvizRepository.getPitanjeByNaziv(naziv)
//    }



//    fun getPitanjaByPitanjeKviz(kviz: String) : List<Pitanje> {
//        var listaPitanjeKviz = pitanjeKvizViewModel.getPitanjeKvizByKviz(kviz)
//
//        var listaPitanja = getPitanja()
//        var listaVrati: MutableList<Pitanje> = ArrayList()
//        for(a in listaPitanjeKviz) {
//            for (b in listaPitanja) {
//                if(a.naziv==b.naziv)
//                    listaVrati.add(b)
//            }
//        }
//        return listaVrati
//    }

    suspend fun getPitanjaByKviz(idKviza: Int) : List<Pitanje>? {
        return PitanjeKvizRepository.getPitanja(idKviza)
    }
}