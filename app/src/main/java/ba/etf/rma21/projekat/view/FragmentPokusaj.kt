package ba.etf.rma21.projekat.view


import android.annotation.SuppressLint
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class FragmentPokusaj(var listaPitanja: List<Pitanje>) : Fragment() {

    private lateinit var navigationView: NavigationView
    private var pitanjaListViewModel = PitanjeListViewModel()
    private var listaFragmenata: MutableList<FragmentPitanje> = mutableListOf()
    var trenutnaPozicija = -1
    private var fragmentPoruka = FragmentPoruka()
    private lateinit var bundle: Bundle
    private val pitanjeKvizViewModel = PitanjeKvizViewModel()
    private val fragmentKvizovi = FragmentKvizovi.newInstance()
    var nazivKviza = ""
    var brojacRezultat : Double= 0.0
    var kvizPredat: Boolean = false
    val scope = CoroutineScope(Job() + Dispatchers.Main)


    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.pokusaj_fragment, container, false)


        if(kvizPredat) {
            bundle = requireArguments()
            val bundle: Bundle = requireArguments()
            nazivKviza = bundle.get("naziv").toString()
            (activity as MainActivity?)!!.navBarKvizoviIPredmeti()


            navigationView = view.findViewById(R.id.navigacijaPitanja)

            var brojac = 0

            scope.launch {
                for (l in pitanjeKvizViewModel.getPitanja(1)!!) {

                    if (listaPitanja.size != 0) {
                        navigationView.menu.add("${brojac + 1}")
                        changeItemColor(brojac)
                        brojac++
                    }
                }
            }

            navigationView.invalidate()
            navigationView.menu.add("Rezultat")
            openFragmentRezultat(brojacRezultat)

            navigationView.setNavigationItemSelectedListener { menuItem -> // set item as selected to persist highlight
                menuItem.isChecked = true
                if (menuItem.title.toString() != "Rezultat")
                    selectItemPredat(menuItem.title.toString().toInt())
                else {
                    openFragmentRezultat(brojacRezultat)
                }
                true
            }
        }

        else {
            bundle = requireArguments()
          //  val bundle: Bundle = requireArguments()
            nazivKviza = bundle.get("naziv").toString()


            //promjena itema u navigationBaru
            (activity as MainActivity?)!!.navBarPokusaj()
            var bottomNav = (activity as MainActivity?)!!.getNavBar()

            navigationView = view.findViewById(R.id.navigacijaPitanja)

            var brojac = 0


            scope.launch {


                for (l in pitanjeKvizViewModel.getPitanja(1)!!) {

                    if (listaPitanja.size != 0) {
                        navigationView.menu.add("${brojac + 1}")
                        listaFragmenata.add(FragmentPitanje.newInstance(listaPitanja[brojac]))
                        brojac++
                    }

                }
                navigationView.invalidate()
            }
            //listener za bottom navigation view
            bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

            navigationView.setNavigationItemSelectedListener { menuItem -> // set item as selected to persist highlight
                menuItem.isChecked = true
                if (menuItem.title.toString() != "Rezultat")
                    selectItem(menuItem.title.toString().toInt())
                else {

                    openFragmentRezultat(brojacRezultat)
                }
                true
            }
        }
        return view
    }



    private fun selectItem(position: Int) {
        if (listaPitanja.size != 0)
            openFragmentPitanje(listaFragmenata[position-1], position)
    }

    private fun selectItemPredat(position: Int) {
        if (listaPitanja.size != 0)
            openFragment(listaFragmenata[position-1])
    }


    private fun openFragmentPitanje(fragment: Fragment, position: Int) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.framePitanje, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

        if (trenutnaPozicija!=-1) {
            if(listaFragmenata[trenutnaPozicija].odgovoreno)
                changeItemColor(trenutnaPozicija)
            trenutnaPozicija=position -1
        }
        else trenutnaPozicija = position -1

    }

    private fun openFragment(fragment: Fragment) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.framePitanje, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }


    private fun openFragmentKvizovi() {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.framePitanje, fragmentKvizovi)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun changeItemColor (position: Int) {

        val item = navigationView.menu.getItem(position)
        val s = SpannableString(item.title)
        val boja =
            if (listaFragmenata[position].odgovorenoPitanje)resources.getColor(R.color.yes_green, null)
             else resources.getColor(R.color.no_red, null)
        s.setSpan(ForegroundColorSpan(boja), 0, s.length, 0)
        item.title = s

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.predajKviz -> {
                for (i in listaFragmenata) {
                    if (i.odgovorenoPitanje)
                        brojacRezultat += 1.0
                }
                navigationView.menu.add("Rezultat")
                openFragmentRezultat(brojacRezultat)

                (activity as MainActivity?)!!.navBarKvizoviIPredmeti()
                kvizPredat = true

                for(i in listaFragmenata) {
                    if(i.listViewInicijaliziran) {
                        i.setListViewNull()
                    }
                }
            }
            R.id.zaustaviKviz -> {
                //Toast.makeText(requireContext(),"Kliknuto zaustavi kviz",Toast.LENGTH_SHORT).show()
                openFragment(fragmentKvizovi)
            }

            R.id.kvizovi -> {
                openFragmentKvizovi()
            }

            R.id.predmeti -> {
                openFragment(FragmentPredmeti.newInstance())
            }
        }
        false
    }


    private fun openFragmentRezultat (brojac: Double) {
        var brojPitanja: Double = listaFragmenata.size.toDouble()
        var procenat : Double = (brojac/brojPitanja) * 100
        val bundle = Bundle()
        bundle.putString("fragment", "pokusaj")
        bundle.putString("kviz", nazivKviza)
        bundle.putDouble("procenat", procenat)

        fragmentPoruka.setArguments(bundle);
        openFragment(fragmentPoruka)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                openFragment(fragmentKvizovi)
            }
        })
    }
}

