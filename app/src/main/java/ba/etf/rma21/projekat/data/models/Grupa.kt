package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//data class Grupa(
//    val id: Int,
//    val naziv: String,
//    @SerializedName("PredmetId") val pid: Int
//    ) {
//
//    override fun toString(): String {
//        return naziv
//    }
//}
//


@Entity
data class Grupa (
    @PrimaryKey @SerializedName("id") var id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv" )var naziv: String,
    @ColumnInfo(name = "PredmetId") @SerializedName("PredmetId" )var pid: Int
        ) {

}