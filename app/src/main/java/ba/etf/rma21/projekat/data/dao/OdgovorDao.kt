package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {

    @Query("DELETE from Odgovor")
    suspend fun izbrisiSve()

    @Insert
    suspend fun insertOdgovor(odgovor:Odgovor)
}