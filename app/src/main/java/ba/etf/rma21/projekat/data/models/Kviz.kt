package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//data class Kviz(
//    val id:Int,
//    val naziv: String,
//    val datumPocetak: Date,
//    val datumKraj: Date,
//    val trajanje: Int,
//    var listaPredmeta: List<Predmet>//,
//   // val nazivPredmeta: String
//) {
//
//}


@Entity
data class Kviz (

    @PrimaryKey @SerializedName("id") var id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") var naziv : String,
    @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetak") var datumPocetka :String,
    @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj") var datumKraj : String,
    @ColumnInfo(name = "trajanje") @SerializedName("trajanje") var trajanje : Int,
    @ColumnInfo(name = "nazivPredmeta") @SerializedName("nazivPredmeta") var nazivPredmeta : String

) {
}