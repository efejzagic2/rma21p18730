package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PredmetIGrupaRepository {

    companion object {


        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPredmeti(): List<Predmet>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmet()
                return@withContext response
            }
        }

        suspend fun getGrupe(): List<Grupa> ?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllGrupa()
                return@withContext response
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta: Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupeByPredmet(idPredmeta)
                return@withContext response
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int): Boolean? {
            GrupaRepository.addGrupa(idGrupa)
            return true
        }

        suspend fun getUpisaneGrupe(): List<Grupa> ?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupeForStudent(AccountRepository.acHash)
                return@withContext response
            }
        }

        suspend fun getPredmetiForGodina(godina: Int): List<Predmet> {
            return  withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmet()

                Log.d("TAGER getPredmetiForGodina" , response.toString())
                return@withContext response.filter {
                    it.godina == godina
                }
            }
        }

//        suspend fun getNazivePredmetaForGodina(godina: Int) : List<String> {
//            return withContext(Dispatchers.IO) {
//                var response = ApiAdapter.retrofit.getAllPredmet()
//
//                return@withContext response
//            }
//        }

        suspend fun getPredmetForString(string: String): Predmet {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmet()

                return@withContext response.first {
                    it.naziv == string
                }
            }
        }



        suspend fun getGrupeForPredmet(idPredmeta: Int) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupeByPredmet(idPredmeta)
                return@withContext response
            }
        }



//
//    fun getUpisaneGrupe():List<Grupa> {
//        return emptyList()
//    }


    }
}