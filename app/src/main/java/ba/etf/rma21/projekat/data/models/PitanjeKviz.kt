package ba.etf.rma21.projekat.data.models

class PitanjeKviz(
        val naziv: String,
        val kviz: String)

{

    companion object {

        var pitanjeKvizLista: List<PitanjeKviz> = ArrayList()

        init {
            pitanjeKvizLista = giveItToMe()
        }

         fun giveItToMe(): List<PitanjeKviz> {

            return listOf(
                    PitanjeKviz("Pitanje 1" , "RMA"),
                    PitanjeKviz("Pitanje 2" , "RMA"),
                    PitanjeKviz("Pitanje 3" , "RMA"),
                    PitanjeKviz("Pitanje 4" , "RMA"),

                    PitanjeKviz("Pitanje 1" , "TP"),
                    PitanjeKviz("Pitanje 2" , "TP"),
                    PitanjeKviz("Pitanje 3" , "TP"),
                    PitanjeKviz("Pitanje 5" , "TP")
            )
        }
         fun getPitanjeKvizByKviz(kviz: String?) : List<PitanjeKviz> {
            return pitanjeKvizLista.filter{
                it.kviz==kviz
            }
        }

        fun getPitanjeKvizByNaziv(naziv: String?) : List<PitanjeKviz> {
            return pitanjeKvizLista.filter{
                it.naziv==naziv
            }
        }


    }


}