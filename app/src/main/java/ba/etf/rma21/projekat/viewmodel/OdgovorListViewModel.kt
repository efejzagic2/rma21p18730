package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository

class OdgovorListViewModel {

    suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
        return OdgovorRepository.getOdgovoriKviz(idKviza)
    }
}