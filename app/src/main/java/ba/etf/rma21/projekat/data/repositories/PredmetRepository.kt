package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PredmetRepository {
    companion object {
//
//        var upisaniPredmeti : MutableList<Predmet> = ArrayList()
//        var predmeti : MutableList<Predmet> = ArrayList()
//
//        init {
//            upisaniPredmeti.add(Predmet("RMA", 2))
//            upisaniPredmeti.add(Predmet("TP", 1))
//
//
//            predmeti.add(Predmet("SI", 3))
//            predmeti.add(Predmet("RMA", 2))
//            predmeti.add(Predmet("WT" , 3))
//            predmeti.add(Predmet("OOAD", 2))
//            predmeti.add(Predmet("TP", 1))
//            predmeti.add(Predmet("IM2", 1))
//        }

//        fun getUpisani(): List<Predmet> {
//            return upisaniPredmeti
//        }
//
private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

       suspend fun getAll(): List<Predmet> { //predmetigrupa repo
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmet()
                return@withContext response
            }
        }

        suspend fun getPredmetId(idPredmeta: Int): Predmet {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getPredmetById(idPredmeta)
                return@withContext response
            }
        }


//
//        fun getNeupisani(): List<Predmet> {
//            return predmeti.minus(upisaniPredmeti)
//        }
//
//        //funkcija vraca listu predmeta prema godini
//        fun getPredmetsByGodina(godina: Int) : List<Predmet> {
//
//            var listaPredmeta : MutableList<Predmet> = ArrayList()
//
//            for(predmet in predmeti) {
//                if(predmet.godina==godina)
//                    listaPredmeta.add(predmet)
//            }
//            return listaPredmeta.minus(upisaniPredmeti)
//        }

        //dodavanje predmeta u listu Upisanih
//        fun addUpisaniPredmet(predmet: Predmet) {
//            upisaniPredmeti.add(predmet)
//        }
    }
}