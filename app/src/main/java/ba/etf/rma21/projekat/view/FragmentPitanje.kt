package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje


class FragmentPitanje(var pitanje: Pitanje): Fragment() {


    lateinit var listView: ListView
    var odgovorenoPitanje = false
    private var savedViewInstance: View? = null
    var odgovoreno = false
    var listViewInicijaliziran = false


    companion object {
        fun newInstance(pitanje: Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if (savedViewInstance != null) {
            savedViewInstance
        } else {
            savedViewInstance = inflater.inflate(R.layout.pitanje_fragment, container, false)
            var tekstPitanja = savedViewInstance!!.findViewById<TextView>(R.id.tekstPitanja)

            tekstPitanja.setText(pitanje.tekstPitanja)
            listView = savedViewInstance!!.findViewById(R.id.odgovoriLista)
            //var odgovori: List<String> = pitanje.opcije
            var odgovori: List<String> = emptyList()
            listViewInicijaliziran = true

            listView.adapter = ArrayAdapter(
                requireContext(),
                android.R.layout.simple_list_item_1,
                odgovori
            )


            listView.setOnItemClickListener(OnItemClickListener { parent, view, position, id ->
            if (position == pitanje.tacan ) {
                listView.getChildAt(position).setBackgroundResource(R.color.yes_green)
                odgovorenoPitanje = true
                odgovoreno=true

                listView.setOnItemClickListener(null)

            } else {
                listView.getChildAt(position).setBackgroundResource(R.color.no_red)
                listView.getChildAt(pitanje.tacan ).setBackgroundResource(R.color.yes_green)
                odgovorenoPitanje = false
                odgovoreno=true


                listView.setOnItemClickListener(null)
            }
        })
            savedViewInstance
        }
    }

    fun setListViewNull() {
        if(listViewInicijaliziran) {
            listView.setOnItemClickListener(null)
        }
    }

}