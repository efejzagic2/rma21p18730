package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class KvizRepository {

    companion object {

        /* val date : Date = Calendar.getInstance().time
         var sdf = SimpleDateFormat("dd.MM.yyyy")
         var kvizovi : List<Kviz> = ArrayList()
         init {
             kvizovi = giveItToMe()
         }

          fun giveItToMe(): List<Kviz> {

             return listOf(

                 Kviz("Kviz 1" , "RMA" , sdf.parse("07.04.2021"), sdf.parse("08.04.2021"), sdf.parse("07.04.2021"),
                     20, "Grupa 1", null),
                 Kviz("Kviz 1" , "RMA" , sdf.parse("08.04.2021"), sdf.parse("09.04.2021"), sdf.parse("08.04.2021"),
                         20, "Grupa 2", null),
                 Kviz("Kviz 1" , "RMA" , sdf.parse("09.04.2021"), sdf.parse("10.04.2021"), sdf.parse("09.04.2021"),
                         20, "Grupa 3", null),

                     //buduci kviz
                 Kviz("Kviz 2" , "OOAD" , sdf.parse("11.04.2021"), sdf.parse("13.04.2021"), sdf.parse("12.04.2021"),
                     20, "Grupa 1", null),
                 Kviz("Kviz 2" , "OOAD" , sdf.parse("12.04.2021"), sdf.parse("14.04.2021"), sdf.parse("13.04.2021"),
                         20, "Grupa 2", null),
                 Kviz("Kviz 2" , "OOAD" , sdf.parse("13.04.2021"), sdf.parse("15.04.2021"), sdf.parse("14.04.2021"),
                         20, "Grupa 3", null),


                  //prosao nije radjen
                 Kviz("Kviz 2" , "IM2" , sdf.parse("12.02.2021"), sdf.parse("14.02.2021"), sdf.parse("13.02.2021"),
                     20, "Grupa 1", null),
                 Kviz("Kviz 2" , "IM2" , sdf.parse("11.02.2021"), sdf.parse("13.02.2021"), sdf.parse("12.02.2021"),
                         20, "Grupa 2", null),


                 Kviz("Kviz 3" , "TP" , sdf.parse("04.04.2021"), sdf.parse("06.04.2021"), sdf.parse("05.04.2021"),
                     20, "Grupa 1", 13F),
                 Kviz("Kviz 3" , "TP" , sdf.parse("05.04.2021"), sdf.parse("07.04.2021"), sdf.parse("06.04.2021"),
                         20, "Grupa 2", null),
                 Kviz("Kviz 3" , "TP" , sdf.parse("06.04.2021"), sdf.parse("08.04.2021"), sdf.parse("07.04.2021"),
                         20, "Grupa 3", null),


                 Kviz("Kviz 3" , "WT" , sdf.parse("07.02.2021"), sdf.parse("07.02.2021"), sdf.parse("07.02.2021"),
                             20, "Grupa 1", null),
                 Kviz("Kviz 3" , "WT" , sdf.parse("08.02.2021"), sdf.parse("08.02.2021"), sdf.parse("08.02.2021"),
                         20, "Grupa 2", null),
                 Kviz("Kviz 3" , "WT" , sdf.parse("09.02.2021"), sdf.parse("09.02.2021"), sdf.parse("09.02.2021"),
                         20, "Grupa 3", null),


                 Kviz("Kviz 3" , "SI" , sdf.parse("07.03.2021"), sdf.parse("07.03.2021"), sdf.parse("07.03.2021"),
                         20, "Grupa 1", null),
                 Kviz("Kviz 3" , "SI" , sdf.parse("08.03.2021"), sdf.parse("08.03.2021"), sdf.parse("08.03.2021"),
                         20, "Grupa 2", null),
                 Kviz("Kviz 3" , "SI" , sdf.parse("09.03.2021"), sdf.parse("09.03.2021"), sdf.parse("09.03.2021"),
                         20, "Grupa 3", null)

                 )
         }

         */


        //        fun getMyKvizes(): List<Kviz> {
//            return kvizovi.filter {
//                GrupaRepository.upisaneGrupe.any { it2: Grupa ->
//                    it2.naziv == it.nazivGrupe && it.nazivPredmeta == it2.nazivPredmeta
//                }
//            }.toList()
//        }

        //spirala 4
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }


        suspend fun getAllKivzDB(context: Context) : List<Kviz> {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getAllKvizDB()
                return@withContext kvizovi
            }
        }




        //promjena za spiralu 3 potrebna
        suspend fun getAll(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllKviz()
                return@withContext response
            }
        }

        suspend fun getKvizById(idKviza: Int): Kviz? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizById(idKviza)
                return@withContext response
            }
        }

        //?
        suspend fun getKvizoviByGrupa(idGrupe: Int): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizoviByGrupa(idGrupe)
                return@withContext response
            }
        }

        suspend fun getUpisani(): List<Predmet>? { //upisani predmeti
            return withContext(Dispatchers.IO) {
                var sviPredmeti = PredmetIGrupaRepository.getPredmeti()
                var lista: MutableList<Predmet> = mutableListOf()
                var upisaneGrupe = PredmetIGrupaRepository.getUpisaneGrupe()

                for(i in upisaneGrupe!!) {
                    for(j in sviPredmeti!!) {
                        if(i.pid == j.id)
                            lista.add(j)
                    }
                }
                return@withContext lista
            }
        }

        suspend fun getDone(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var response = getAll()
//                return@withContext response.filter {
//                    it.datumKraj.after(Calendar.getInstance().time) //jos su samo bodovi kriterij
//                }
                return@withContext response
            }
        }


        suspend fun getNotTaken(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var response = getAll()
//                return@withContext response.filter {
//                    it.datumKraj.after(Calendar.getInstance().time) //jos su samo bodovi kriterij
//                }
                return@withContext response
            }


        }


        suspend fun getFuture(): List<Kviz> {
            val date : Date = Calendar.getInstance().time
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllKviz()
//                response.filter {
//                    it.datumPocetak.before(date)
//                }.toList()

                return@withContext response
            }
        }

        suspend fun getMyKvizes() : List<Kviz>{
            return withContext(Dispatchers.IO) {
                if(AccountRepository.getHash()!="test") {
                    val responseGrupa =
                        ApiAdapter.retrofit.getGrupeForStudent(AccountRepository.getHash())

                    var mojKvizovi: MutableList<Kviz> = mutableListOf<Kviz>()
                    var lista: List<Kviz> = listOf<Kviz>()
                    for (i in responseGrupa) {
                        lista = ApiAdapter.retrofit.getKvizoviByGrupa(i.id)
                        for (j in lista) {
                            mojKvizovi.add(j)
                        }
                    }
                    var response = mojKvizovi.toList()
                    return@withContext response
                }
                else {
                    return@withContext getAll()
                }
            }
        }






//        fun getDone(): List<Kviz> {
//            return getMyKvizes().filter {
//                it.osvojeniBodovi!=null && it.datumPocetka.before(date)
//            }
//        }
//
//        fun getFuture(): List<Kviz> {
//            return getMyKvizes().filter {
//                it.datumPocetka.after(date)
//            }
//        }
//
//        fun getNotTaken(): List<Kviz> {
//            return getMyKvizes().filter {
//                it.osvojeniBodovi == null && it.datumKraj.before(date)
//            }
//        }
//
//        //spirala 3
//        fun getByID(id:Int): Kviz {
//            return  Kviz("Test GetById" , "IM2" , sdf.parse("11.02.2021"), sdf.parse("13.02.2021"), sdf.parse("12.02.2021"),
//                20, "Grupa 2", null)
//        }
//
//        //vraća listu svih kvizova za grupe na kojima je student upisan
//        fun getUpisani():List<Kviz> {
//            return emptyList()
//        }
//    }
    }
}