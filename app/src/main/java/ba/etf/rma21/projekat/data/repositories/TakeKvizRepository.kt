package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*


class TakeKvizRepository {

    companion object {


        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        //post
        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                val response =
                    ApiAdapter.retrofit.addKvizTaken(
                        AccountRepository.acHash,
                        idKviza
                    )
                return@withContext response;
            }
        }


        //get
        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizTakenForStudent(AccountRepository.acHash)
                if(response.size>0)
                    return@withContext response
                return@withContext null
            }
        }

//        suspend fun getKvizTakenForStudent(acHash: String) : List<KvizTaken> {
//            return withContext(Dispatchers.IO) {
//                var response = ApiAdapter.retrofit.getKvizTakenForStudent(acHash)
//                Log.d("TAGER", response.toString())
//                return@withContext response
//            }
//        }

    }

}