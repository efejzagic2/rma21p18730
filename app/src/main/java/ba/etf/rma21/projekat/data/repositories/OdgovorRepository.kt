package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.OdgPost
import ba.etf.rma21.projekat.data.models.Odgovor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.round

class OdgovorRepository {

    companion object {

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor> ?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getOdgovoriForStudent(
                    AccountRepository.getHash(),
                    idKviza
                )
                return@withContext response
            }        }

        //post
        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int) : Int?{
            return withContext(Dispatchers.IO) {

                val pitanja = PitanjeKvizRepository.getPitanja(TakeKvizRepository.getPocetiKvizovi()!!.firstOrNull(){
                    it.id == idKvizTaken
                }!!.id!!)

                var tacnoOdgovoreni = pitanja!!.filter {
                    it.id == idPitanje && it.tacan == odgovor
                }.toList().size

                var bodovi = round(tacnoOdgovoreni.toDouble()/pitanja.size*100).toInt()


                val response = ApiAdapter.retrofit.addOdgovor(
                    AccountRepository.getHash(),
                    idKvizTaken,
                    OdgPost(idPitanje, odgovor, bodovi)
                )


                return@withContext bodovi
            }
        }

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
    }
}