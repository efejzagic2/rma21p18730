package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiAdapter
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class AccountRepository {

    companion object {

        var acHash: String = "8cea3025-7376-43f3-920f-06f3768a81d3"

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }


        suspend fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash

            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                db!!.accountDao().izbrisiSve()
                db!!.accountDao().insertAccount(Account(acHash, SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").format(
                    Calendar.getInstance().time)))
                return@withContext true
            }
        }





        suspend fun getHash(): String {
            return acHash
        }

        suspend fun getAccountByHash(acHash: String): Account {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAccount(acHash)
                return@withContext response
            }
        }

        suspend fun deleteAcoount() : String {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.deleteAccount(AccountRepository.getHash())
                return@withContext response
            }
        }


    }
}