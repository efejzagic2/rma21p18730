package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class KvizListViewModel : ViewModel() {

    suspend fun odaberiKviz(pos: Int) : List<Kviz>? {
        return when (pos) {
            0 -> KvizRepository.getMyKvizes()
            1 -> KvizRepository.getAll()
            2 -> KvizRepository.getDone()
            3 -> KvizRepository.getFuture()
            4 -> KvizRepository.getNotTaken()
            else -> null
        }
    }

    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getAllKvizDB(context: Context, onSuccess: (kviz: List<Kviz>) -> Unit,
                 onError: () -> Unit){

        scope.launch{
            val result = KvizRepository.getAllKivzDB(context)
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }


    suspend fun getAll (): List<Kviz> {
        return KvizRepository.getAll()
    }

    suspend fun getKvizoviByGrupa(idGrupe: Int) : List<Kviz> {
        return KvizRepository.getKvizoviByGrupa(idGrupe)
    }

    suspend fun getKvizById(idKviza: Int): Kviz ?{
        return KvizRepository.getKvizById(idKviza)
    }

    suspend fun getUpisani(): List<Predmet>? {
        return KvizRepository.getUpisani()
    }
//    fun getKvizByName(naziv: String): Kviz? {
//        return KvizRepository.getMyKvizes().find {
//            it.nazivPredmeta==naziv
//        }
//    }
}