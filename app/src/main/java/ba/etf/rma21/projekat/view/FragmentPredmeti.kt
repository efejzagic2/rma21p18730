package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.viewmodel.GrupaListViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetiListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class FragmentPredmeti : Fragment() {

    private lateinit var odabirGodina: Spinner
    private lateinit var odabirPredmet: Spinner
    private lateinit var odabirGrupa: Spinner
    private lateinit var dodajPredmetDugme : Button

    private var fragmentPoruka = FragmentPoruka()
    private val fragmentKvizovi = FragmentKvizovi.newInstance()
    private val grupaListViewModel = GrupaListViewModel()
    private val predmetListViewModel = PredmetiListViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.predmeti_fragment, container, false)

        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)


        ArrayAdapter.createFromResource(view.context, R.array.odabir_godina, android.R.layout.simple_spinner_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            odabirGodina.adapter = adapter
        }


        //onItemSeleceted implementacija
        odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                napuniSpinnerPredmet(pos)
            }
            override fun onNothingSelected(parent: AdapterView<out Adapter>?) {}
        }


        if(odabirPredmet.isEmpty()) isprazniSpinnerGrupa()
        odabirPredmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                napuniSpinnerGrupa(odabirPredmet.selectedItem.toString())
            }
            override fun onNothingSelected(parent: AdapterView<out Adapter>?) {

            }
        }

        //dodajDugme Listener
        dodajPredmetDugme.setOnClickListener {
            if(odabirPredmet.isEmpty() || odabirGrupa.isEmpty())
                Toast.makeText(requireContext(), "Molimo da odaberete predmet i grupu", Toast.LENGTH_LONG).show()
            else {

                scope.launch {
                    //PredmetRepository.addUpisaniPredmet(
//                        Predmet(
//                            odabirPredmet.selectedItem.toString(),
//                            odabirGodina.selectedItem.toString().toInt()
//                        )
//                    )
                    //var info = getGroupInfo(odabirPredmet.toString(), odabirGrupa.toString())

                    var id = grupaListViewModel.getGrupaByName(odabirGrupa.selectedItem.toString())!!.id


                    grupaListViewModel.addGrupa(
                         id!!
                    )
                }
                //slanje podataka u FragmentPoruka
                val bundle = Bundle()
                bundle.putString("predmet", odabirPredmet.selectedItem.toString())
                bundle.putString("grupa", odabirGrupa.selectedItem.toString())
                fragmentPoruka.setArguments(bundle);

               // pokretanje Fragmenta
                openFragment(fragmentPoruka)
            }
        }
        return view
    }



//    //metoda za dobijanje informaciji o grupi za post
//    private fun getGroupInfo(odabirPredmet: String, odabirGrupa: String) : Int{
//        var vrati = -1
//        val scope = CoroutineScope(Job() + Dispatchers.Main)
//        var grupe = emptyList<Grupa>()
//        scope.launch {
//            grupe = grupaListViewModel.getGrupeByPredmet(vratiPredmet(odabirPredmet).id)
//
//
//        var grupa = vratiGrupu(odabirGrupa)
//
//            for (i in grupe) {
//                if(grupa.naziv == i.naziv)
//                    vrati = i.id
//            }
//        }
////        if(vrati==-1) {
////
////        }
//        return vrati
//    }
//
//    private fun vratiPredmet (string: String) : Predmet {
//        val scope = CoroutineScope(Job() + Dispatchers.Main)
//
//        var predmet = Predmet(-1, "test", 2020)
//        scope.launch {
//             predmet = predmetListViewModel.getPredmete().find {
//                 it.naziv == string
//             }!!
//        }
//        if(predmet.id==-1) {
//
//        }
//
//        return predmet
//    }
//
//    private fun vratiGrupu (string: String) : Grupa {
//        val scope = CoroutineScope(Job() + Dispatchers.Main)
//
//        var grupa = Grupa(-1, "test", 2)
//        scope.launch {
//            grupa = grupaListViewModel.getAll().find { //provjeriti da li treba sve grupe ili samo po id
//                it.naziv == string
//            }!!
//        }
//        if(grupa.id==-1) {
//
//        }
//
//        return grupa
//    }
//

    private fun napuniSpinnerGrupa(string: String) {

        val scope = CoroutineScope(Job() + Dispatchers.Main)
        var predmeti = emptyList<Predmet>()

        var novaLista: MutableList<String> = mutableListOf()
        scope.launch {
            predmeti  = PredmetRepository.getAll()

            var predmetString = PredmetIGrupaRepository.getPredmetForString(string)
            var grupe = PredmetIGrupaRepository.getGrupeForPredmet(predmetString.id)


            for(i in grupe) {
                novaLista.add(i.naziv)
            }

//            for (predmet in predmeti) {
//                if (predmet.naziv == string) {
//                    novaLista.clear()
////                    for (l in GrupaRepository.getGroupsByPredmet(predmet.naziv)) {
////                        novaLista.add(l.naziv)
////                    }
//
//                    for (l in GrupaRepository.getGrupeByPredmet(predmet.id)) {
//                        novaLista.add(l.naziv)
//                    }


                    var adapter2 = ArrayAdapter(
                        requireContext(),
                        android.R.layout.simple_list_item_1,
                        novaLista
                    )
                    odabirGrupa.adapter = adapter2
                }
            }
//        }
//    }

    private fun napuniSpinnerPredmet(pos: Int) {
//        var lista = PredmetRepository.getPredmetsByGodina(pos + 1)
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        var lista = emptyList<Predmet>()
        var novaLista : MutableList<String> = mutableListOf()
        scope.launch {
            lista = PredmetIGrupaRepository.getPredmetiForGodina(pos+1)

            for(i in lista) {
                novaLista.add(i.naziv)
            }

            Log.d("TAGER naupni spinner" , lista.toString())
//            lista.filter {
//                it.godina == pos+1
//            }

         //   var novaLista: MutableList<String> = ArrayList()
//            for (l in lista) {
//                novaLista.add(l.naziv)
//            }



            var adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, novaLista)
            odabirPredmet.adapter = adapter
        }
        if(lista.isEmpty()) isprazniSpinnerGrupa()

    }

    private fun isprazniSpinnerGrupa() {
        var list = ArrayList<String>()
        list.clear()
        var adapterEmpty = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, list)
        odabirGrupa.adapter = adapterEmpty
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                openFragment(fragmentKvizovi)
            }
        })
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}