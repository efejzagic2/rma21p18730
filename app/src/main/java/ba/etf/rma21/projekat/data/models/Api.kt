package ba.etf.rma21.projekat.data.models

import retrofit2.http.*


interface Api {

    @GET("/account/{id}/lastUpdate?=date")
    suspend fun isChanged(@Path("id") id:String, @Query("date") date:String) : Boolean

    //predmet GET
    @GET("predmet")
    suspend fun getAllPredmet() : List<Predmet> //Vraća sve predmete

    @GET("predmet/{id}")
    suspend fun getPredmetById(@Path("id")id: Int): Predmet //Vraća predmet sa zadanim id-em

    //grupa GET
    @GET("grupa")
    suspend fun getAllGrupa() : List<Grupa>

    @GET("kviz/{id}/grupa")
    suspend fun getGrupeByKviz(@Path("id")id: Int): List<Grupa> //Vraća grupe u kojima je kviz dostupan

    @GET("/student/{id}/grupa")
    suspend fun getGrupeForStudent(
        @Path("id") acHash: String
    ): List<Grupa> //Vraća grupe u koje je student upisan

    @GET("grupa/{id}")
    suspend fun getGrupaById(@Path("id")id: Int) : Grupa //Vraća grupu sa zadanim id-em

    @GET("predmet/{id}/grupa")
    suspend fun getGrupeByPredmet(@Path("id")id: Int): List<Grupa> //Vraća listu grupa na predmetu sa zadanim id-em

    //grupa POST - treba dodati
    @POST("/grupa/{gid}/student/{id}")
    suspend fun addGrupa(
        @Path("gid")gid: Int,
        @Path("id")acHash: String
    ): Unit

    //kviz GET
    @GET("kviz")
    suspend fun getAllKviz() : List<Kviz>

    @GET("kviz/{id}")
    suspend fun getKvizById(@Path("id")id: Int) : Kviz //Vraća kviz sa id-em ili poruku da kviz ne postoji

    @GET("grupa/{id}/kvizovi")
    suspend fun getKvizoviByGrupa(@Path("id")id: Int): List<Kviz> //Vraća kvizove koji su dodijeljeni ovoj grupi

    //odgovor GET
    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovoriForStudent(@Path("id") acHash: String, @Path("ktid") ktid: Int): List<Odgovor>

    //odgovor POST

    @Headers("accept: application/json", "Content-Type: application/json")
    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun addOdgovor(
        @Path("id") acHash: String,
        @Path("ktid") ktid: Int,
        @Body odgPost: OdgPost
    ): Unit


    //KvizTaken GET
    @GET("/student/{id}/kviztaken")
    suspend fun getKvizTakenForStudent(
        @Path("id") acHash: String
    ) : List<KvizTaken> //Vraća listu pokušaja za kvizove studenta sa zadanim hash id-em

    //KvizTaken POST
    @POST("/student/{id}/kviz/{kid}")
    suspend fun addKvizTaken(
        @Path("id") acHash: String,
        @Path("kid") idKviza: Int
    ): KvizTaken


    //Account GET
    @GET("/student/{id}")
    suspend fun getAccount(@Path("id")acHash: String): Account ///student/{id}

    //Account DELETE = treba dodati

    @DELETE("/student/{id}/upisugrupeipokusaji")
    suspend fun deleteAccount(
        @Path("id") acHash: String
    ): String

    //Pitanje GET
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanja(
        @Path("id") id : Int
    ) : List<Pitanje>

}