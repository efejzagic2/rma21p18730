package ba.etf.rma21.projekat.data.repositories

//Spirala 3
class ApiConfig {
    companion object {
        var baseURL: String = "https://rma21-etf.herokuapp.com"

        fun postaviBaseURL(baseURL: String) {
            this.baseURL = baseURL
        }
    }

}