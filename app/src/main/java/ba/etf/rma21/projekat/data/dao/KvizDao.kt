package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface  KvizDao {

    @Query("SELECT * FROM kviz")
    suspend fun getAllKvizDB () : List<Kviz>

    @Query("DELETE from Kviz")
    suspend fun izbrisiSve()

    @Insert
    suspend fun insertKviz(vararg kviz: Kviz)



}