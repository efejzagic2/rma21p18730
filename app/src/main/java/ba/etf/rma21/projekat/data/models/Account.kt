package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//class Account(val id: Int, val student: String, val acHash: String) {
//
//
//    public fun getHash() :String {
//        return this.acHash
//    }
//}


@Entity
data class Account(
    @PrimaryKey @SerializedName("acHash") var acHash: String,
    @ColumnInfo(name = "lastUpdate") @SerializedName("lastUpdate") var lastUpdate: String


    ) {

}