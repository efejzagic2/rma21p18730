package ba.etf.rma21.projekat.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class KvizListAdapter(
        private var kvizovi: List<Kviz>,
        private val onItemClicked: (kviz: Kviz) -> Unit

) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.kviz_kartica, parent, false)
        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        val scope = CoroutineScope(Job() + Dispatchers.Main)

        var kvizTaken: KvizTaken? = null
        scope.launch {
           // kvizTaken = getKvizTaken(kvizovi[position])
        }


        scope.launch {
            holder.nazivPredmeta.text = popuniListuPredmeta(kvizovi[position])

//            if (kvizovi[position].nazivPredmeta.isNullOrEmpty()) holder.nazivPredmeta.text =
//                "NullOrEmpty"
//            else if (kvizovi[position].listaPredmeta.size == 1) holder.nazivPredmeta.text =
//                kvizovi[position].listaPredmeta[0].naziv
//            else if (kvizovi[position].listaPredmeta.size > 1) holder.nazivPredmeta.text =
//                dodajZarezNaVisePredmeta(kvizovi[position].listaPredmeta)
//            else holder.nazivPredmeta.text = "NIsta"

        }

         //   holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta
            holder.kviz.text = kvizovi[position].naziv
            holder.datumKviz.text = setDatumKviza(kvizovi[position])
            holder.vrijemeKviz.text = kvizovi[position].trajanje.toString() + " min"
            holder.itemView.setOnClickListener { onItemClicked(kvizovi[position]) }

//        if(kvizovi[position].osvojeniBodovi!=null)
//            holder.bodovi.text = kvizovi[position].osvojeniBodovi.toString()
//        else
        if(kvizTaken==null) {
            holder.bodovi.text = "."
        }
        else {
            holder.bodovi.text = kvizTaken!!.osvojeniBodovi.toString()
        }

        for (kviz in kvizovi) {
            scope.launch {
                var str = setImageColor(kvizovi[position])

                if (str == "blue")
                    holder.bojaKviz.setImageResource(R.drawable.blue)
                else if (str == "green")
                    holder.bojaKviz.setImageResource(R.drawable.green)
                else if (str == "yellow")
                    holder.bojaKviz.setImageResource(R.drawable.yellow)
                else if (str == "red")
                    holder.bojaKviz.setImageResource(R.drawable.red)
                else
                    holder.bojaKviz.setImageResource(R.drawable.card)
            }
        }
    }

//    private suspend fun getKvizTaken(kviz: Kviz): KvizTaken? {
//        return TakeKvizRepository.getPocetiKvizovi()!!.firstOrNull() {
//            it.id == kviz.id
//        }
//    }
//    private fun dodajZarezNaVisePredmeta(listaPredmeta: List<Predmet>) : String {
//
//        var stringReturn = ""
//        for (i in listaPredmeta.indices) {
//            stringReturn += listaPredmeta[i].naziv
//            if(i != listaPredmeta.size - 1) stringReturn+=","
//        }
//        return stringReturn
//    }

    private suspend fun popuniListuPredmeta(kviz: Kviz): String {
        var predmeti =  GrupaRepository.getGrupeByKviz(kviz.id).map {
            PredmetRepository.getPredmetId(it.pid)
        }.toSet().toList()

        return vratiString(predmeti)
    }

    private suspend fun vratiString(listaPredmeta: List<Predmet>) : String{
        var nazivPredmetaString = ""
        for(i in listaPredmeta.indices) {
            nazivPredmetaString += listaPredmeta[i].naziv
            if(i <listaPredmeta.size-1)
                nazivPredmetaString += ","

        }
        return nazivPredmetaString
    }

    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nazivPredmeta: TextView = itemView.findViewById(R.id.textPredmet)
        val kviz: TextView = itemView.findViewById(R.id.textKviz)
        val datumKviz: TextView = itemView.findViewById(R.id.textDatumKviz)
        val vrijemeKviz: TextView = itemView.findViewById(R.id.textVrijemeKviz)
        val bodovi : TextView = itemView.findViewById(R.id.textBodovi)
        val bojaKviz: ImageView = itemView.findViewById(R.id.imageDot)
    }

    private fun setDatumKviza(kviz : Kviz) : String? {
        val date : Date = Calendar.getInstance().time
        var sdf = SimpleDateFormat("dd.MM.yyyy")
        var sdfNovi = SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss")
//        return if(kviz.datumRada.before(date) && kviz.datumRada.after(kviz.datumPocetka)) {
//            sdf.format(kviz.datumRada).toString()
//        } else if(kviz.datumRada.after(date) && date.after(kviz.datumPocetka) && date.before(kviz.datumKraj)) {
//            sdf.format(kviz.datumKraj).toString()
//        }
//        return if(date.before(sdfNovi.parse(kviz.datumPocetak)) /*&& date.before(kviz.datumKraj)*/) {
//            sdfNovi.format(kviz.datumPocetak).toString()
////        } else if(date.after(kviz.datumKraj)) {
////            sdf.format(kviz.datumKraj).toString()
//        } else
             return kviz.datumPocetka
        //return "datum"
    }

    private suspend fun setImageColor(kviz: Kviz) : String {
        var string = ""
            val date: Date = Calendar.getInstance().time
       // val kvizTaken = getKvizTaken(kviz)

//            if (kviz.osvojeniBodovi==null && kviz.datumPocetak.after(date)) string = "yellow"
//            else if (kviz.osvojeniBodovi!=null ) string = "blue"
//            else if (kviz.osvojeniBodovi==null && kviz.datumKraj.after(date) && kviz.datumPocetak.before(date)) string = "green"
//            else if (kviz.datumKraj.before(date) && kviz.osvojeniBodovi==null) string = "red"


        //ovaj ovdje
//        if  (kviz.datumPocetak.after(date)) string = "yellow"
//        else if(kviz.datumKraj!=null) {
//            if (kviz.datumKraj!!.after(date) && kviz.datumPocetak.before(date)) string = "green"
//            else if (kviz.datumKraj.before(date)) string = "red"
//        }
//        else  string = "blue"

//        if(kvizTaken!!.osvojeniBodovi==null && kviz.datumPocetak.after(date)) string = "yellow"
//        else if(kvizTaken!!.osvojeniBodovi!=null) string = "blue"
//        if(kviz.datumKraj!=null) {
//            if (kvizTaken!!.osvojeniBodovi == null && kviz.datumPocetak.before(date) && kviz.datumKraj.after(date)) string = "green"
//            else if (kviz.datumKraj.before(date) && kvizTaken!!.osvojeniBodovi==null) string = "red"
//        }
        return string
    }
}