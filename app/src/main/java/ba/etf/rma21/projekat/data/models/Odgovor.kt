package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//
//class Odgovor(
//    val id: Int, val odgovoreno: Int) {
//
//}



@Entity
data class Odgovor(
    @PrimaryKey @SerializedName("id") var id: Int,
    @ColumnInfo(name = "odgovoreno") @SerializedName("odgovoreno") var odgovoreno : Int,
    @ColumnInfo(name = "PitanjeId") @SerializedName("PitanjeId") var pitanjeId : Int,
    @ColumnInfo(name = "KvizId") @SerializedName("KvizId") var kvizId : Int,
    @ColumnInfo(name = "KvizTakenId") @SerializedName("KvizTakenId") var kvizTakenId : Int

) {

}